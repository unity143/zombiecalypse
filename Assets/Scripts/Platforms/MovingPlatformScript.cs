using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatformScript : MonoBehaviour
{
    [SerializeField] Vector2 targetA;
    [SerializeField] Vector2 targetB;
    [SerializeField] float speed = 1;
    
    Rigidbody2D platformBody;
    private Vector2 targetPosition;
    private Transform playerTransformParent;

    // Start is called before the first frame update
    void Start()
    {
        platformBody = GetComponent<Rigidbody2D>();
        targetPosition = targetA;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Vector2 currentPosition = platformBody.transform.position;

        platformBody.transform.position = Vector2.MoveTowards(currentPosition, targetPosition, speed * Time.deltaTime);
        if (Vector2.Distance(currentPosition, targetA) < 0.1f)
        {
            targetPosition = targetB;
        }
        else if (Vector2.Distance(currentPosition, targetB) < 0.1f)
        {
            targetPosition = targetA;
        }             
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        // If collides with player
        if (collision.gameObject.layer == 8) {
            // Save original player Transform
            playerTransformParent = collision.gameObject.transform.parent;
            collision.gameObject.transform.parent = transform;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        // If collides with player
        if (collision.gameObject.layer == 8)
        {
            collision.gameObject.transform.parent = playerTransformParent;
            playerTransformParent = null;
        }
    }
}
