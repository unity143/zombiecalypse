using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

public class PlayerScript : MonoBehaviour
{

    [SerializeField] float speed = 10;
    [SerializeField] float speedJump = 10;
    [SerializeField] float maxHitpoints;
    [SerializeField] float maxDaggers;
    [SerializeField] float damage;
    [SerializeField] float killDelay;
    [SerializeField] GameObject daggerPrefab;
    [SerializeField] UnityEvent deathEvents;

    bool isJumping;
    bool isAttacking;
    bool isThrowing;
    float direction;

    Rigidbody2D playerBody;
    Collider2D playerCollider;
    SpriteRenderer playerSpriteRenderer;
    Animator playerAnimator;
    GameObject enemy;

    private bool _isDead;
    public bool IsDead { get { return _isDead; } }

    private float _hitpoints;
    public float Hitpoints { get { return _hitpoints; } }

    private float _daggers;
    public float Daggers { get { return _daggers; } }


    // Start is called before the first frame update
    void Start()
    {
        isJumping = false;
        isAttacking = false;
        isThrowing = false;
        _isDead = false;
        direction = 0;
        _hitpoints = maxHitpoints;
        _daggers = maxDaggers;
        playerBody = GetComponent<Rigidbody2D>();
        playerCollider = GetComponent<Collider2D>();
        playerSpriteRenderer = GetComponent<SpriteRenderer>();
        playerAnimator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Vector2 playerVelocity = playerBody.velocity;
        bool touchingGround = playerCollider.IsTouchingLayers(LayerMask.GetMask("Tiles")) || playerCollider.IsTouchingLayers(LayerMask.GetMask("Moving Platforms")) || playerCollider.IsTouchingLayers(LayerMask.GetMask("Enemy"));

        // Check HP
        if (_isDead)
        {
            // When dead, make Enemies ignore player

            playerAnimator.SetBool("isDead", true);
            StartCoroutine(isKilled(killDelay));
            return;
        }

        //Attack
        if (isAttacking)
        {
            isAttacking = false;
            playerAnimator.SetBool("isAttacking", false);
        }
        // Throw dagger
        if (isThrowing && touchingGround)
        {
            isThrowing = false;
            playerAnimator.SetBool("isThrowing", false);
        }
        // Jumping
        if (isJumping && touchingGround)
        {
            playerVelocity.y = speedJump;
            isJumping = false;
            playerAnimator.SetBool("isJumping", false);
        }
        // Falling
        playerAnimator.SetBool("isFalling", !touchingGround);

        // Running
        playerVelocity.x += direction * speed * Time.fixedDeltaTime;
        playerBody.velocity = playerVelocity;
    }

    #region Collisions

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Enemies"))
            if (collision.gameObject.TryGetComponent<EnemyScript>(out var script))
            {
                if (script != null && !script.IsDead)
                {
                    enemy = collision.gameObject;
                }
            }
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Enemies"))
            if (collision.gameObject.GetComponent<EnemyScript>().IsDead)
                enemy = null;
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.layer == 11)
            enemy = null;
    }

    #endregion Collisions

    #region Input
    private void OnJump()
    {
        bool touchingGround = playerCollider.IsTouchingLayers(LayerMask.GetMask("Tiles")) || playerCollider.IsTouchingLayers(LayerMask.GetMask("Moving Platforms"));
        if (touchingGround)
        {
            isJumping = true;
            playerAnimator.SetBool("isJumping", true);
        }
    }

    private void OnAttack()
    {
        isAttacking = true;
        AudioManager.Instance.PlaySoundOneShot("sword_1");
        playerAnimator.SetBool("isAttacking", true);
    }

    private void OnThrow()
    {
        bool touchingGround = playerCollider.IsTouchingLayers(LayerMask.GetMask("Tiles")) || playerCollider.IsTouchingLayers(LayerMask.GetMask("Moving Platforms"));
        bool hasDaggerInInventory = _daggers > 0;

        if (touchingGround && hasDaggerInInventory)
        {
            isThrowing = true;
            AudioManager.Instance.PlaySoundOneShot("dagger_1");
            playerAnimator.SetBool("isThrowing", true);
            _daggers--;
        }
    }

    private void OnMove(InputValue input)
    {
        direction = input.Get<float>();
        if (direction < 0)
            playerSpriteRenderer.flipX = true;
        else if (direction > 0)
            playerSpriteRenderer.flipX = false;

        playerAnimator.SetFloat("isRunning", Math.Abs(direction));
    }

    #endregion Input

    #region Behaviour

    private void ThrowDagger()
    {
        Transform playerPosition = playerBody.transform;
        Transform levelPosition = playerPosition.transform.parent.transform;
        // Create Dagger object, set its parent as "level", set right direction
        var dagger = Instantiate(daggerPrefab, playerPosition);
        dagger.GetComponent<SpriteRenderer>().flipX = playerSpriteRenderer.flipX;
        dagger.transform.parent = levelPosition.transform;
    }

    // Is called during Animation: Attack
    private void ComputeAttack()
    {
        if (enemy != null)
        {
            if (enemy.TryGetComponent<EnemyScript>(out var script))
            {
                if (script.IsDead) {
                    this.enemy = null;
                }
                else
                {
                    script.HurtEnemy(damage);
                    Debug.Log($"Player dealt {damage} HP damage.");
                }
            }
        }
    }

    private IEnumerator isKilled(float killDelay)
    {
        yield return new WaitForSeconds(killDelay);
        deathEvents?.Invoke();
        gameObject.SetActive(false);
    }

    public void HurtPlayer(float damageDealt)
    {
        _hitpoints -= damageDealt;
        _isDead = _hitpoints <= 0;
        Debug.Log($"Player's HP: {_hitpoints} ");
    }

    public float getHealthRatio()
    {
        return Math.Clamp(_hitpoints / maxHitpoints, 0, 1f);
    }

    #endregion Behaviour

    #region Sounds

    public void PlayFootstep()
    {
        System.Random random = new System.Random();       
        switch(random.Next(1, 3))
        {
            case 1:
                AudioManager.Instance.PlaySoundOneShot("foot_1");
                break;
            case 2:
                AudioManager.Instance.PlaySoundOneShot("foot_2");
                break;
            case 3:
                AudioManager.Instance.PlaySoundOneShot("foot_3");
                break;
        }       
    }

    #endregion Sounds

}
