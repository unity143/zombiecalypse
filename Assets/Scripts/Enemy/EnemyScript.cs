using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class EnemyScript : MonoBehaviour
{
    Rigidbody2D enemyBody;
    SpriteRenderer enemySprite;
    Animator enemyAnimator;

    // dummy AI    
    bool canSeePlayer;
    bool canAttackPlayer;

    private bool _isDead;
    public bool IsDead { get { return _isDead; } }
    public bool IsNotDead => !IsDead;

    private bool _isKilled;
    public bool IsKilled { get { return _isKilled; } }


    private float _hitpoints;
    public float Hitpoints { get { return _hitpoints; } }

    // Enemy stats

    [SerializeField] float enemySpeed;
    [SerializeField] float max_hitpoints;
    [SerializeField] float damage;

    [SerializeField] Vector2 leftBorder;
    [SerializeField] Vector2 rightBorder;

    [SerializeField] GameObject enemyPrefab;
    [SerializeField] GameObject particles;

    private Vector2 targetPosition;
    private GameObject player;


    // Start is called before the first frame update
    void Start()
    {
        enemyBody = GetComponent<Rigidbody2D>();
        enemySprite = GetComponent<SpriteRenderer>();
        enemyAnimator = GetComponent<Animator>();
        _isDead = false;
        _isKilled = false;
        canSeePlayer = false;
        canAttackPlayer = false;
        targetPosition = rightBorder;
        _hitpoints = max_hitpoints;
    }

    void BeginSeePlayer(GameObject inputPlayer)
    {
        canSeePlayer = true;
        player = inputPlayer;
    }

    void EndSeePlayer()
    {
        canSeePlayer = false;
        player = null;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        // Dead
        if (_isKilled)
        {
            _isDead = true;
            _isKilled = false;
            AudioManager.Instance.PlaySoundOneShot("zombie_death_1");
            enemyAnimator.SetBool("isDead", true);
            Invoke("DisableAfterDeath", 2);
            Invoke("ReviveEnemy", 5);
            return;
        }
        if (_isDead)
        {
            return;
        }
        // Walk between borders if no player present
        if (!canSeePlayer && !canAttackPlayer)
        {
            CheckPosition();
            MoveRandomly();
            return;
        }

        // If player present, go towards it
        if (canSeePlayer && !canAttackPlayer)
        {
            CheckPosition();
            PlayAmbientSound();
            GoTowardsPlayer();
            return;
        }
        // If colliding with player, start attack
        if (canSeePlayer && canAttackPlayer)
        {
            CheckPosition();
            AttackPlayer();
            return;
        }
    }

    #region Collisions
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            if (collision.gameObject.TryGetComponent<PlayerScript>(out var script))
            {
                if (script.IsDead)
                {
                    EndSeePlayer();
                }
                else
                {
                    canAttackPlayer = true;
                }
            }
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            canAttackPlayer = false;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision) {

        if (collision.gameObject.layer == LayerMask.NameToLayer("Player") && !collision.gameObject.GetComponent<PlayerScript>().IsDead)
        {
            BeginSeePlayer(collision.gameObject);
        }
        else
        {
            EndSeePlayer();
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            EndSeePlayer();
        }
    }
    #endregion Collisions

    #region Behaviour

    private void GoTowardsPlayer()
    {
        Vector2 currentPosition = enemyBody.transform.position;
        if (Vector2.Distance(currentPosition, player.transform.position) > 0.1f)
        {
            enemyBody.transform.position = Vector2.MoveTowards(currentPosition, player.transform.position, enemySpeed * Time.deltaTime);
            enemyAnimator.SetBool("isWalking", true);
        }
        else
        {
            enemyAnimator.SetBool("isWalking", false);
        }
    }

    private void MoveRandomly()
    {
        Vector2 currentPosition = enemyBody.transform.position;
        enemyBody.transform.position = Vector2.MoveTowards(currentPosition, targetPosition, enemySpeed * Time.deltaTime);
        enemyAnimator.SetBool("isWalking", true);
        if (Vector2.Distance(currentPosition, rightBorder) < 0.1f)
        {
            enemyAnimator.SetBool("isWalking", false);
            targetPosition = leftBorder;
        }
        else if (Vector2.Distance(currentPosition, leftBorder) < 0.1f)
        {
            enemyAnimator.SetBool("isWalking", false);
            targetPosition = rightBorder;
        }
    }

    private void CheckPosition()
    {
        Vector2 currentPosition = enemyBody.transform.position;
        // Moving randomly, no player present
        if (!canSeePlayer && !canAttackPlayer)
        {
            if (targetPosition == leftBorder)
                enemySprite.flipX = true;
            else if (targetPosition == rightBorder)
                enemySprite.flipX = false;
            return;
        }
        // See player, flip towards it based on players Position
        if (canSeePlayer)
        {
            if (currentPosition.x < player.transform.position.x)
                enemySprite.flipX = false;
            else if (currentPosition.x > player.transform.position.x)
                enemySprite.flipX = true;
        }
    }

    private void AttackPlayer()
    {
        if (player != null)
        {
            if (player.TryGetComponent<PlayerScript>(out var script))
            {
                if (script.IsDead)
                {
                    EndSeePlayer();
                }
                else
                {
                    PlayAttackSound();
                    enemyAnimator.SetBool("isAttacking", true);
                }
            }
        }
    }
    private void PlayAmbientSound()
    {
        System.Random random = new System.Random();
        switch (random.Next(1, 2))
        {
            case 1:
                AudioManager.Instance.PlaySoundIfNotPlaying("zombie_ambient_1");
                break;
            case 2:
                AudioManager.Instance.PlaySoundIfNotPlaying("zombie_ambient_2");
                break;
        }
    }

    private void PlayAttackSound()
    {
        System.Random random = new System.Random();
        switch (random.Next(1, 2))
        {
            case 1:
                AudioManager.Instance.PlaySoundIfNotPlaying("zombie_attack_1");
                break;
            case 2:
                AudioManager.Instance.PlaySoundIfNotPlaying("zombie_attack_2");
                break;            
        }
    }

    // Is called during Animation: Attack
    private void ComputeAttack()
    {
        if (player != null)
        {
            if (player.TryGetComponent<PlayerScript>(out var script))
            {
                if (script.IsDead)
                {
                    EndSeePlayer();
                }
                else
                {
                    script.HurtPlayer(damage);
                    Debug.Log($"Enemy dealt {damage} HP damage.");
                }
            }
        }
        enemyAnimator.SetBool("isAttacking", false);
    }

    public void HurtEnemy(float damageDealt)
    {
        _hitpoints -= damageDealt;
        _isKilled = _hitpoints <= 0;
        Debug.Log($"Enemy's HP: {_hitpoints}");
    }

    private void DisableAfterDeath()
    {
        EventManager.Instance.TriggerEvent("IncreaseKill");        
        // Play ExperienceParticles
        var emission = particles.GetComponent<ParticleSystem>().emission;
        var delay = particles.GetComponent<ParticleSystem>().main.duration;
        emission.enabled = true;
        particles.GetComponent<ParticleSystem>().Play();
        //emission.enabled = false;
        Invoke(nameof(DeactivateEnemy), delay);
    }

    private void DeactivateEnemy()
    {
        var emission = particles.GetComponent<ParticleSystem>().emission;
        emission.enabled = false;
        gameObject.SetActive(false);
    }

    private void ReviveEnemy()
    {
        Vector2 originalEnemyPosition = gameObject.transform.position;
        Transform originalEnemyParentPosition = gameObject.transform.parent;

        Vector2 originalLeftBorder = leftBorder;
        Vector2 originalRightBorder = rightBorder;
        GameObject enemyClone = (GameObject)Instantiate(enemyPrefab);
        enemyClone.transform.position = originalEnemyPosition;
        enemyClone.transform.parent = originalEnemyParentPosition;
        enemyClone.gameObject.GetComponent<EnemyScript>().ChangeBorders(originalLeftBorder, originalRightBorder);
        enemyClone.SetActive(true);
        Destroy(gameObject);
    }

    public void ChangeBorders(Vector2 newLeftBorder, Vector2 newRightBorder)
    {
        this.leftBorder = newLeftBorder;
        this.rightBorder = newRightBorder;
    }

    public float getHealthRatio()
    {
        return Math.Clamp(_hitpoints / max_hitpoints, 0, 1f);
    }

    #endregion Behaviour


}
