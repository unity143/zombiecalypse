using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySoundScript : MonoBehaviour
{
    public void OnTriggerStay2D(Collider2D collision)
    {
        System.Random random = new System.Random();
        switch (random.Next(1, 2))
        {
            case 1:                
                AudioManager.Instance.PlaySoundIfNotPlaying("zombie_ambient_1");
                break;
            case 2:
                AudioManager.Instance.PlaySoundIfNotPlaying("zombie_ambient_2");
                break;               
        }
    }

    public void OnTriggerExit2D(Collider2D collision)
    {
        AudioManager.Instance.StopSound("zombie_ambient_1");
        AudioManager.Instance.StopSound("zombie_ambient_2");
    }




}
