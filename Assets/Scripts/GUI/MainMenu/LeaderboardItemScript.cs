using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class LeaderboardItemScript : MonoBehaviour
{
    [SerializeField] GameObject playerName;
    [SerializeField] GameObject savedAt;
    [SerializeField] GameObject playerExperience;
    [SerializeField] GameObject playerKills;
    [SerializeField] GameObject playerCoins;

    public void FillLeaderboardItemTexts(LeaderboardItem item)
    {
        FillPlayerName(item.PlayerName);
        FillSavedAt(item.ItemSavedAt);
        FillPlayerExperience(item.PlayerExperience);
        FillPlayerKills(item.PlayerKills);
        FillPlayerCoins(item.PlayerCoins);
    }

    private void FillPlayerName(string playerName)
    {
        this.playerName.GetComponent<TextMeshProUGUI>().text = playerName;
    }

    private void FillSavedAt(DateTime savedAt)
    {
        this.savedAt.GetComponent<TextMeshProUGUI>().text = savedAt.ToString("dd.MM.yyyy");
    }

    private void FillPlayerExperience(int playerExperience)
    {
        this.playerExperience.GetComponent<TextMeshProUGUI>().text = playerExperience.ToString();
    }

    private void FillPlayerKills(int playerKills)
    {
        this.playerKills.GetComponent<TextMeshProUGUI>().text = playerKills.ToString();
    }

    private void FillPlayerCoins(int playerCoins)
    {
        this.playerCoins.GetComponent<TextMeshProUGUI>().text = playerCoins.ToString();
    }


}
