using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuCanvasScript : MonoBehaviour
{
    [SerializeField] GameObject mainMenu;
    [SerializeField] GameObject controlsMenu;
    [SerializeField] GameObject leaderboardMenu;
    [SerializeField] GameObject saveMenu;

    public void OpenMainMenu()
    {
        mainMenu.SetActive(true);
    }

    public void CloseMainMenu()
    {
        mainMenu.SetActive(false);
    }

    public void OpenLeaderboardMenu()
    {
        leaderboardMenu.SetActive(true);
    }

    public void CloseLeaderboardMenu()
    {
        leaderboardMenu.SetActive(false);
    }

    public void OpenSaveMenu()
    {
        saveMenu.SetActive(true);
    }
    
    public void CloseSaveMenu()
    {
        saveMenu.SetActive(false);
    }

    public void OpenControlsMenu()
    {
        controlsMenu.SetActive(true);
    }

    public void CloseControlsMenu()
    {
        controlsMenu.SetActive(false);
    }
}
