using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeaderboardMenuScript : MonoBehaviour
{
    [SerializeField] GameObject canvas;
    public void OnExitClick()
    {
        Debug.Log("Close LEADERBOARD Menu.");
        canvas.GetComponent<MainMenuCanvasScript>().CloseLeaderboardMenu();
        canvas.GetComponent<MainMenuCanvasScript>().OpenMainMenu();
    }
}
