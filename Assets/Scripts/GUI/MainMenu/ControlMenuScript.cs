using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlMenuScript : MonoBehaviour
{
    [SerializeField] GameObject canvas;
    public void OnExitClick()
    {
        Debug.Log("Close CONTROLS Menu.");
        canvas.GetComponent<MainMenuCanvasScript>().CloseControlsMenu();
        canvas.GetComponent<MainMenuCanvasScript>().OpenMainMenu();
    }
}
