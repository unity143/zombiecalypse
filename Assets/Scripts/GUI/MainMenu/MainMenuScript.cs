using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using static UnityEngine.GraphicsBuffer;

public class MainMenuScript : MonoBehaviour
{
    [SerializeField] Camera mainCamera;
    [SerializeField] GameObject canvas;
    [SerializeField] GameObject player;

    public void OnNewClick()
    {
        Debug.Log("Click on NEW button");
        ResumeLevel();
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void OnSaveClick()
    {
        Debug.Log("Click on SAVE button");
        canvas.GetComponent<MainMenuCanvasScript>().CloseMainMenu();
        canvas.GetComponent<MainMenuCanvasScript>().OpenSaveMenu();
    }

    public void OnLeaderboardClick()
    {
        Debug.Log("Click on LEADERBOARD button");
        canvas.GetComponent<MainMenuCanvasScript>().CloseMainMenu();
        canvas.GetComponent<MainMenuCanvasScript>().OpenLeaderboardMenu();
    }

    public void OnControlsClick()
    {
        Debug.Log("Click on Controls button");
        canvas.GetComponent<MainMenuCanvasScript>().CloseMainMenu();
        canvas.GetComponent<MainMenuCanvasScript>().OpenControlsMenu();

    }
    public void OnTwitterClick()
    {
        Debug.Log("Click on Twitter button");
        Application.OpenURL("https://twitter.com/mm_cybersin");

    }

    public void OnExitClick()
    {
        Debug.Log("Click on EXIT button");
        ResumeLevel();
        gameObject.SetActive(false);

    }

    private void ResumeLevel()
    {
        Time.timeScale = 1.0f;
        player.GetComponent<PlayerInput>().enabled = true;
    }


}
