using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class SaveMenuScript : MonoBehaviour
{
    [SerializeField] GameObject canvas;
    [SerializeField] GameObject leaderboard;
    [SerializeField] GameObject playerName;
    public void OnExitClick()
    {
        Debug.Log("Close SAVE Menu.");
        Exit();
    }

    public void OnSaveClick()
    {
        Debug.Log("Click on SAVE button");
        Save();
        Exit();
    }

    private void Exit()
    {
        canvas.GetComponent<MainMenuCanvasScript>().CloseSaveMenu();
        canvas.GetComponent<MainMenuCanvasScript>().OpenMainMenu();
    }

    private void Save()
    {
        // Add playerName to playerPrefs and save it to leaderboard
        if (playerName.GetComponent<TMP_InputField>().text != "")
        {
            leaderboard.GetComponent<LeaderboardScript>().SavePlayerNameSavedAtToPlayerPrefs(playerName.GetComponent<TMP_InputField>().text);
            leaderboard.GetComponent<LeaderboardScript>().AddCurrentPlayerToLeaderboard();
        }
        else
        {
            Debug.Log("No name, no save.");
            // Add error message window.
        }
    }
}
