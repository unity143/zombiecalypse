using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms.Impl;

public class LeaderboardScript : MonoBehaviour
{  

    [SerializeField] GameObject leaderBoardItemPrefab;
    [SerializeField] GameObject leaderboardScrollObject;

    private List<LeaderboardItem> leaderboardItems = new List<LeaderboardItem>();
    private string playerPrefsLeaderboardString = "";

    public void Start()
    {
        ReloadLeaderboard();
    }

    public void ReloadLeaderboard()
    {
        leaderboardItems.Clear();
        LoadPlayerPrefsLeaderboard();
        LoadLeaderboardItemsFromString(playerPrefsLeaderboardString);
        FillLeaderboardWithItems();
    }

    public void SavePlayerNameSavedAtToPlayerPrefs(string playerName)
    {
        PlayerPrefs.SetString("current_playerName", playerName);
        PlayerPrefs.SetString("current_savedAt", DateTime.Now.ToString());
    }

    public void AddCurrentPlayerToLeaderboard()
    {
        // Reload leaderboard
        LoadPlayerPrefsLeaderboard();
        LoadLeaderboardItemsFromString(playerPrefsLeaderboardString);

        var playerItem = new LeaderboardItem(playerExperience: PlayerPrefs.GetString("current_playerExperience", "0"),
                                             playerCoins: PlayerPrefs.GetString("current_playerCoins", "0"),
                                             playerKills: PlayerPrefs.GetString("current_playerKills", "0"),
                                             playerName: PlayerPrefs.GetString("current_playerName", "ANONYMOUS"),
                                             itemSavedAt: PlayerPrefs.GetString("current_savedAt", "1900-01-01"));
        leaderboardItems.Add(playerItem);        
        // create new leaderboard string
        var leaderboardString = "";
        foreach(var item in leaderboardItems)
        {
            leaderboardString += item.ToString() + ";";
        }
        PlayerPrefs.SetString("leaderboard", leaderboardString);
    }

    private void LoadPlayerPrefsLeaderboard()
    {
        // Clear leaderboard and load from PlayerPrefs
        playerPrefsLeaderboardString = "";
        playerPrefsLeaderboardString = PlayerPrefs.GetString("leaderboard", "10,0,1,cybersin,2023-01-01;20,10,1,test,2023-01-02");
    }

    private void LoadLeaderboardItemsFromString(string saveString)
    {
        var itemsAsString = saveString.Split(";");
        foreach (var item in itemsAsString)
        {
            if (item != "")
            { 
                var splitItem = item.Split(",");
                leaderboardItems.Add(new LeaderboardItem(playerExperience: splitItem[0],
                                                            playerCoins: splitItem[1],
                                                            playerKills: splitItem[2],
                                                            playerName: splitItem[3],
                                                            itemSavedAt: splitItem[4]));
            }            
        }
    }

    private void FillLeaderboardWithItems()
    {
        leaderboardItems.Sort((x, y) => y.PlayerExperience.CompareTo(x.PlayerExperience));
        foreach ( var item in leaderboardItems)
        {
            // create game object, fill fields and push it as child into scrollbar
            GameObject leaderboardItemClone = Instantiate(leaderBoardItemPrefab);
            leaderboardItemClone.GetComponent<LeaderboardItemScript>().FillLeaderboardItemTexts(item);
            leaderboardItemClone.transform.SetParent(leaderboardScrollObject.transform);
            leaderboardItemClone.transform.localScale = new Vector3(1, 1, 1);
            leaderboardItemClone.transform.localPosition = new Vector3(0, 0, 0);
        }
    }
}

public class LeaderboardItem
{
    private int playerExperience;
    private int playerCoins;
    private int playerKills;
    private string playerName;
    private DateTime itemSavedAt;

    public LeaderboardItem(string playerExperience, string playerCoins, string playerKills, string playerName, string itemSavedAt)
    {
        this.playerExperience = int.TryParse(playerExperience, out var resultPlayerExperience) ? resultPlayerExperience : 0;
        this.playerCoins = int.TryParse(playerCoins, out var resultPlayerCoins) ? resultPlayerCoins : 0;
        this.playerKills = int.TryParse(playerKills, out var resultPlayerKills) ? resultPlayerKills : 0;
        this.playerName = playerName;
        this.itemSavedAt = DateTime.TryParse(itemSavedAt, out var resultItemSavedAt) ? resultItemSavedAt : new DateTime();
    }

    public override string ToString()
    {
        string[] result = { this.playerExperience.ToString(), ",", 
                            this.playerCoins.ToString(), ",",
                            this.playerKills.ToString(), ",",
                            this.playerName.ToString(), ",",
                            this.itemSavedAt.ToString("yyyy-MM-dd") };
        return string.Concat(result);
    }

    public string PlayerName => playerName;
    public int PlayerExperience => playerExperience;
    public int PlayerCoins => playerCoins;
    public int PlayerKills => playerKills;
    public DateTime ItemSavedAt => itemSavedAt;
}

