using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class DaggerBarScript : MonoBehaviour
{
    [SerializeField] GameObject player;

    private TextMeshProUGUI _daggerText;

    // Start is called before the first frame update
    void Start()
    {
        _daggerText = GetComponent<TextMeshProUGUI>();
    }

    // Update is called once per frame
    void Update()
    {
        UpdateDaggerCount();
    }

    private void UpdateDaggerCount()
    {
        _daggerText.text = player.gameObject.GetComponent<PlayerScript>().Daggers.ToString();
    }
}
