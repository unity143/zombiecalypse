using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CoinBarScripts : MonoBehaviour
{
    [SerializeField] GameObject levelManager;

    private TextMeshProUGUI _coinText;

    // Start is called before the first frame update
    void Start()
    {
        _coinText = GetComponent<TextMeshProUGUI>();
    }

    // Update is called once per frame
    void Update()
    {
        UpdateCoinCount();
    }

    private void UpdateCoinCount()
    {
        _coinText.text = levelManager.gameObject.GetComponent<LevelManager>().CoinCount.ToString();
    }
}
