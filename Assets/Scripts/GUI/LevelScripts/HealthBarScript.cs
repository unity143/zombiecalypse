using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBarScript : MonoBehaviour
{
    [SerializeField] GameObject player;

    private Transform _healthBarSlicerTransform;
    // Start is called before the first frame update
    void Start()
    {
        _healthBarSlicerTransform = gameObject.GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        UpdateHealth();
    }

    private void UpdateHealth()
    {
        var new_scale = player.GetComponent<PlayerScript>().getHealthRatio();
        _healthBarSlicerTransform.localScale = new Vector3(new_scale, 1, 1);
    }
}
