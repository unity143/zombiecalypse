using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class LevelMenuScript : MonoBehaviour
{

    [SerializeField] GameObject canvas;
    [SerializeField] GameObject levelManager;
    [SerializeField] GameObject player;
    [SerializeField] GameObject mainMenu;

    public void OnMenuClick()
    {
        StartCoroutine(DelayedPause());        
        Debug.Log("Click on MENU button");
        SaveLevelStatsToPlayerPrefs();      
        player.GetComponent<PlayerInput>().enabled = false;
        mainMenu.gameObject.SetActive(true);
    }

    private void SaveLevelStatsToPlayerPrefs()
    {
        PlayerPrefs.SetString("current_playerCoins", levelManager.GetComponent<LevelManager>().CoinCount.ToString());
        PlayerPrefs.SetString("current_playerKills", levelManager.GetComponent<LevelManager>().Kills.ToString());
        PlayerPrefs.SetString("current_playerExperience", levelManager.GetComponent<LevelManager>().Experience.ToString());
    }

    private IEnumerator DelayedPause()
    {
        yield return null;
        Time.timeScale = 0;
    }


}
