using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class IntroScript : MonoBehaviour
{
    [SerializeField] GameObject startButton;

    public void Start()
    {
        startButton.SetActive(false);
    }
    public void OnStart()
    {
        SceneManager.LoadScene(1);
        Debug.Log("Opening Level Scene...");
    }

    public void OnAssetButton()
    {
        Application.OpenURL("https://www.gameart2d.com/");
    }

    public void OnFontButton()
    {
        Application.OpenURL("http://www.vicfieger.com/");
    }

    public void OnIntroButton()
    {
        Application.OpenURL("https://midjourney.com/home/?callbackUrl=%2Fapp%2F");
    }

    public void OnAudiorezoutButton()
    {
        Application.OpenURL("https://audiorezoutlink.biglink.to/gvhF");
    }
}
