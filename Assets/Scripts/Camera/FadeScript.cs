using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class FadeScript : MonoBehaviour
{
    [SerializeField] float fadingDuration;
    [SerializeField] UnityEvent finishEvent;
    [SerializeField] Color startColor;
    [SerializeField] Color endColor;

    private bool _isFading = false;
    private SpriteRenderer spriteRenderer;

    public void OnEnable()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        StartCoroutine(Fade());
    }

    private IEnumerator Fade() 
    {
        if(!_isFading)
        {
            _isFading = true;
            spriteRenderer.color = startColor;
            float timeSpentFading = 0;

            while(timeSpentFading < fadingDuration)
            {
                timeSpentFading += Time.deltaTime;
                spriteRenderer.color = Color.Lerp(startColor, endColor, timeSpentFading / fadingDuration);
                yield return null;
            }
            _isFading = false;
            finishEvent?.Invoke();
        }
    }
}
