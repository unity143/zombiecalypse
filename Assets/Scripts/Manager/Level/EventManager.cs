using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EventManager : MonoBehaviour
{
    private static EventManager _instance;
    private static Dictionary<string, UnityEvent> events = new Dictionary<string, UnityEvent>();

    public static EventManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<EventManager>();
                if (_instance == null)
                    Debug.Log("No EventManager in the scene!");
            }
            return _instance;
        }
    }

    public void StartListening(string eventName, UnityAction action)
    {
        UnityEvent thisEvent;
        if (!events.TryGetValue(eventName, out thisEvent)) 
        {
            thisEvent = new UnityEvent();
            events.Add(eventName, thisEvent);
        }
        thisEvent.AddListener(action);
    }

    public void StopListening(string eventName, UnityAction action) 
    {
        UnityEvent thisEvent;
        if(events.TryGetValue(eventName, out thisEvent)) 
        {
            thisEvent.RemoveListener(action);
        }
    }

    public void TriggerEvent(string eventName) 
    {
        UnityEvent thisEvent;
        if (events.TryGetValue(eventName, out thisEvent))
        {
            thisEvent.Invoke();
        }
        else
        {
            Debug.Log($"This event: {eventName} has no listeners.");
        }
    }

}
