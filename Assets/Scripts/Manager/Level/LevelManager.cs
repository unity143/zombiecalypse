using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    public static LevelManager levelManager;

    [SerializeField] float winScore = 10;
    [SerializeField] float winKillScore = 2;

    [SerializeField] UnityEvent startEvents;
    [SerializeField] UnityEvent deathEvents;
    [SerializeField] float deathDelay;
    [SerializeField] UnityEvent victoryEvents;
    [SerializeField] float victoryDelay;

    private float _coinCount;
    public float CoinCount { get { return _coinCount; } }
   
    private float _totalExperience;
    public float Experience { get { return _totalExperience; } }

    private float _killScore;
    public float Kills { get { return _killScore; } }

    public void Awake()
    {
        if (levelManager != null)
        {
            Destroy(gameObject);
            return;
        }

        _coinCount = 0;
        _killScore = 0;
        _totalExperience = 0;
        levelManager = this;
        DontDestroyOnLoad(gameObject);
        EventManager.Instance.StartListening("IncreaseScore", IncreaseScore);
        EventManager.Instance.StartListening("IncreaseKill", IncreaseKill);
    }

    public void Update()
    {
        // Check for victory conditions
        if (_coinCount >= winScore && _killScore >= winKillScore)
        {
            OnVictoryEvents();
            // NextLevel();
        }
    }

    public void OnStartEvents()
    {
        startEvents?.Invoke();
    }

    public void OnVictoryEvents()
    {
        victoryEvents?.Invoke();
        StartCoroutine(Victory(victoryDelay));
    }

    public void OnDeathEvents()
    {
        deathEvents?.Invoke();
        StartCoroutine(Death(deathDelay));
    }

    private void IncreaseKill()
    {
        _killScore++;
        _totalExperience += 10;
    }

    private void IncreaseScore()
    {
        _coinCount++;
        _totalExperience++;
    }

    public void NextLevel()
    {
        int level = SceneManager.GetActiveScene().buildIndex;
        level++;
        if (level >= SceneManager.sceneCountInBuildSettings)
            level = 0;
        SceneManager.LoadScene(level);
        _coinCount = 0;
    }

    private IEnumerator Death(float deathDelay)
    {
        yield return new WaitForSeconds(deathDelay);
        // Add Game over overlay
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }

    private IEnumerator Victory(float victoryDelay)
    {
        yield return new WaitForSeconds(victoryDelay);
        // Add Victory overlay
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }


}
