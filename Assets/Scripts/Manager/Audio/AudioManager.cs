using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

public class AudioManager : MonoBehaviour
{    
    Dictionary<string, AudioSource> audioSources = new Dictionary<string, AudioSource>();

    private static AudioManager instance;
    public static AudioManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = GameObject.FindObjectOfType<AudioManager>();
                
                if (instance == null)
                {
                    GameObject audioManager = new GameObject(typeof(AudioManager).Name);
                    instance = audioManager.AddComponent<AudioManager>();
                }
            }
            return instance;
        }
    }   

    // Start is called before the first frame update
    void Start()
    {
        if (instance == null)
        {
            instance = this as AudioManager;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
        LoadSounds();        
    }

    private void LoadSounds()
    {
        var sounds = transform.Find("sounds");
        foreach (Transform sound in sounds)
        {
            if (audioSources.ContainsKey(sound.name))
            {
                Debug.LogWarning($"AudioSource already contains sound named {sound.name}!");
            }
            else
            {
                audioSources.Add(sound.name, sound.GetComponent<AudioSource>());
            }
        }
    }
    public void PlaySoundIfNotPlaying(string name)
    {
        if (audioSources.ContainsKey(name))
        {
            if (!IsPlaying(name))
            {
                PlaySound(name);
            }
        }
        else
        {
            Debug.Log($"AudioSource does not contain sound: {name}!");
        }
    }

    public void PlaySound(string name)
    {
        if(audioSources.ContainsKey(name))
        {
            audioSources[name].Play();
        }
        else
        {
            Debug.Log($"AudioSource does not contain sound: {name}!");
        }       
    }

    public void PlaySoundOneShot(string name)
    {
        if(audioSources.ContainsKey(name))
        {
            audioSources[(name)].PlayOneShot(audioSources[(name)].clip);
        }
        else
        {
            Debug.Log($"AudioSOurce does not contain sound: {name}!");
        }
    }

    public bool IsPlaying(string name)
    {
        if (audioSources.ContainsKey(name))
        {
            return audioSources[name].isPlaying;
        }
        else
        {
            Debug.Log($"AudioSOurce does not contain sound: {name}!");
            return false;
        }
    }

    public void StopSound(string name)
    {
        if (audioSources.ContainsKey(name))
        {
            audioSources[name].Stop();
        }
        else
        {
            Debug.Log($"AudioSource does not contain sound: {name}!");
        }
    }
}


[System.Serializable]
public class Sound
{
    [SerializeField] string name;
    [SerializeField] AudioClip clip;
}
