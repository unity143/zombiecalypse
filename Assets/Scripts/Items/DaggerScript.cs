using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DaggerScript : MonoBehaviour
{
    [SerializeField] float daggerSpeed = 3;
    [SerializeField] float daggerDamage = 10;

    SpriteRenderer daggerSpriteRenderer;
    Rigidbody2D daggerBody;
    Collider2D daggerCollider;
    // Start is called before the first frame update
    void Start()
    {
        daggerCollider = GetComponent<Collider2D>();
        daggerSpriteRenderer = GetComponent<SpriteRenderer>();
        daggerBody = GetComponent<Rigidbody2D>();        
    }

    // Update is called once per frame
    void Update()
    {
        // Dagger will move in direction until reach enemy or tile
        Vector2 daggerPosition = daggerBody.position;
        if (daggerSpriteRenderer.flipX == true)
        {
            daggerPosition.x -= daggerSpeed * Time.deltaTime;
        }
        else if (daggerSpriteRenderer.flipX == false)
        {
            daggerPosition.x += daggerSpeed * Time.deltaTime;
        }
        daggerBody.position = daggerPosition;       
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {        
        DealDamage(collision.gameObject);
        Destroy(gameObject);        
    }

    private void DealDamage(GameObject damagedObject)
    {
        if(damagedObject.layer == LayerMask.NameToLayer("Enemies"))
        {
            if(damagedObject.TryGetComponent<EnemyScript>(out var script) && (script?.IsNotDead ?? false))
            {
                script.HurtEnemy(daggerDamage);
            }
        }
    }
}
