using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinScript : MonoBehaviour
{
    [SerializeField] LayerMask layer_mask;
    [SerializeField] GameObject particles;
    

    private SpriteRenderer spriteRenderer;
    private bool _once;

    public void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        _once = true;
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (((1 << collision.gameObject.layer) & layer_mask) != 0 && _once)
        {
            // Increase CoinScore 
            EventManager.Instance.TriggerEvent("IncreaseScore");
            // Hide Object
            Destroy(spriteRenderer);
            // Play ExperienceParticles
            var emission = particles.GetComponent<ParticleSystem>().emission;
            var delay = particles.GetComponent<ParticleSystem>().main.duration;
            emission.enabled = true;
            particles.GetComponent<ParticleSystem>().Play();
            _once = false;
            //Destroy object
            Invoke(nameof(DestroyCoin), delay);      
        }
    }

    private void DestroyCoin()
    {
        Destroy(gameObject);
    }
}
