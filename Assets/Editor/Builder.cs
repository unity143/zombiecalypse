using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEditor;
public static class BuildScript
{
    static void WebGLProductionBuild()
    {
        // Build the player.\
        UnityEditor.BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions();
        buildPlayerOptions.scenes = new[] { "./Assets/Scenes/intro.unity", "./Assets/Scenes/level.unity" };        
        buildPlayerOptions.locationPathName = "./Build";
        buildPlayerOptions.target = BuildTarget.WebGL;
        buildPlayerOptions.options = BuildOptions.None; // set whatever you want here
        BuildPipeline.BuildPlayer(buildPlayerOptions);  // apply the setting changes
    }
}